class CommentsController < ApplicationController
    
    # http_basic_authenticate_with name: "dhh", password: "secret", only: :destroy

    def create
      @article = Article.find(params[:article_id])
      @@comment = @article.comments.create(params[:comment].permit(:body).merge(author_id: current_user.id))
      redirect_to article_path(@article)
    end
  
    def destroy
      @article = Article.find(params[:article_id])
      @comment = @article.comments.find(params[:id])
      authorize @comment
      @comment.destroy
      redirect_to article_path(@article)
    end

  end
  