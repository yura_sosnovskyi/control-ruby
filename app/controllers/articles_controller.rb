class ArticlesController < ApplicationController
  
  # http_basic_authenticate_with name: "dhh", password: "secret", except: [:index, :show]
  before_action :authenticate_user!, only: %i[new create edit update destroy]
  before_action :set_article, only: %i[show edit update destroy]
  # before_action :authorize_user!, only: %i[edit update destroy]



  def index

    # @articles = Article.ordered.with_authors.paginate(:page => params[:page], per_page: 5).where('title LIKE ?', "#{params[:search]}%")
    @sort_type = params.fetch(:sort_type, @sort_type)
    @articles = Article.with_authors.paginate(page: params[:page], per_page: 5).where('title LIKE ?', "#{params[:search]}%")
    @status = params.fetch(:status, @status)
    
    if @status == "Public"
      @articles = @articles.public_articles
    elsif @status == "Private"
      @articles = @articles.private_articles
    elsif @status == "Archived"
      @articles = @articles.archived_articles
    end

    if @sort_type == "title_asc"
      @articles = @articles.ordered_by_title_asc
    elsif @sort_type == "title_desc"
      @articles = @articles.ordered_by_title_desc
    elsif @sort_type == "oldest"
      @articles = @articles.ordered_by_date_asc
    else 
      @articles = @articles.ordered_by_date_desc
    end
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)
    @article.author = current_user

    unless article_params[:image].nil?
      image = Cloudinary::Uploader.upload(article_params[:image], options = {})
    end

    if @article.save
      redirect_to @article
    else
      render :new
    end
  end

  def edit
    authorize @article
    @article = Article.find(params[:id])
  end

  def update
    authorize @article
    @article = Article.find(params[:id])
    unless article_params[:image].nil?
      image = Cloudinary::Uploader.upload(article_params[:image], options = {})
    end

    if @article.update(article_params)
      redirect_to @article
    else
      render :edit
    end
  end

  def destroy
    authorize @article
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to root_path
  end

  private
    def article_params
      params.require(:article).permit(:title, :body, :status, :image)
    end

    def set_article
      @article = Article.find(params[:id])
    end
  
    def authorize_user!
      return if @article.author_id == current_user.id
      redirect_to :articles, alert: "You are not allowed to make this action!"
    end
end
