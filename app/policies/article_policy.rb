class ArticlePolicy < ApplicationPolicy
    def index?
      true
    end
  
    def show?
      true
    end
    def create?
      true
    end
  
    def new?
      true
    end
  
    def update?
      author?
    end
  
    def destroy?
      author?
    end
  
    private
    def author?
      return false unless user
      record.author_id == user.id || user.admin == true
    end
  
  end
  