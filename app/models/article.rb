class Article < ApplicationRecord
    include Visible
  
    has_one_attached :image, dependent: :destroy
    has_many :comments, dependent: :destroy
    belongs_to :author, class_name: 'User'
  
    validates :title, presence: true
    validates :body, presence: true, length: { minimum: 10 }

    scope :ordered_by_title_asc, -> (direction = :asc) { order(title: direction)}
    scope :ordered_by_title_desc, -> (direction = :desc) { order(title: direction)}
    scope :ordered_by_date_asc, -> (direction = :asc) { order(created_at: direction)}
    scope :ordered_by_date_desc, -> (direction = :desc) { order(created_at: direction)}

    scope :public_articles, -> (status = "public"){ where('status=?',"#{status}")}
    scope :private_articles, -> (status = "private"){ where('status=?',"#{status}")}
    scope :archived_articles, -> (status = "archived"){ where('status=?',"#{status}")}

    scope :with_authors, -> {includes(:author)}
  
  end
  