# control-ruby
**What was performed:**
- Getting started (5 points)
- Pagination (2 points)
- Images upload (2 points)
- Images upload to cloud (2 points)
- Bootstrap styles (Additional points)
- Search by article title (2 points)
- Authentication by different users (2 points)
- Authorization (3 points)
- Admin mode (1 point)
- Article sorting by title and date (Additional points)
- Filtering by article status (2 points)
- Deploy on heroku (3 points)

Total: 24 + additional

Link to blog:
https://nedo-icq.herokuapp.com/
